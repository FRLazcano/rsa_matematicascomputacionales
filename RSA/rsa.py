import primes as generator
import random
import os
import shutil

class RSAClient:
    def __init__(self, id, keysize=10):
        self.__keysize=keysize
        self.__id=id
        self.__keys=self.createKeys();
        print('Nodo creado: ' + self.__id)

    def getID(self):
        return self.__id

    def decryptMessage(self, filename='data.txt'):
        decrypted=''
        file=open('Messages/'+filename,'r')
        for line in file:
            num=int(line)
            print(num)
            m=(num**self.__keys[0][1])%self.__keys[0][0]
            m=m%255
            decrypted+=chr(m)
            print(' ' +  str(num) + ' ' + str(m))
        print('Mensaje desencriptado: ' + decrypted)

    def encryptMessage(self, partner , message, filename='data.txt'):
        print('Encriptando mensaje: ' + message)
        key=partner.getPublicKey()
        file=open('Messages/'+filename,'w')
        for c in message:
            c=int(ord(c))
            num=(int(c)**key[1])%key[0]
            print(' ' + str(c) + ' ' + str(num))
            file.write(str(num) + '\n')
        print('encripcion termindada, guardando en:' + filename)

    def getPublicKey(self):
        return self.__keys[1]

    def findCoprime(self,n,size=16):
        while True:
            e = random.randrange(2 ** (size - 1), 2 ** (size))
            if self.gcd(e, n) == 1:
                break
        return e

    def modInverse(self,a,b):
        if self.gcd(a, b) != 1:
            return Noneos.mkdir('Messages')
        u1, u2, u3 = 1, 0, a
        v1, v2, v3 = 0, 1, b

        while v3 != 0:
            q = u3 // v3
            v1, v2, v3, u1, u2, u3 = (u1 - q * v1), (u2 - q * v2), (u3 - q * v3), v1, v2, v3

        return (u1 % b)

    def gcd(self,a,b):
        if b==0:
            return a
        else:
            return self.gcd(b,a%b)

    def createKeys(self):
        primes=[generator.generate_prime_number(self.__keysize),generator.generate_prime_number(self.__keysize)]
        n=primes[0]*primes[1]
        totient=(primes[0]-1)*(primes[1]-1)
        c=self.findCoprime(totient,self.__keysize)
        d=self.modInverse(c,totient)
        private=[n,c]
        public=[n,d]
        return [private,public]

def clear():
    try:
        os.system('clear')
    except:
        os.system('cls')

def printNodes(nodes):
    print('Nodos Disponibles:')
    for node in nodes.keys():
        print('     ' + node)

def printFiles():
    clear()
    print('Archivos Disponibles: ')
    for file in os.listdir('Messages'):
        print('     ' + file)

def addNode(nodes):
    clear()
    try:
        id=input('Introduzca el ID del nuevo nodo: (back para cancelar): ')
        if id=='back':
            return
        nodes[id]=RSAClient(id)
    except:
        print('Ocurrio una excepcion, revise sus entradas')

def removeNode(nodes):
    if len(nodes)==0:
        print('No hay nodos creados')
        return
    clear()
    printNodes(nodes)
    try:
        id=input('Introduzca ID del nodo a borrar (back para cancelar): ')
        if id=='back':
            return
        del nodes[id]
    except:
        print('Ocurrio una excepcion, revise sus entradas')

def encrypt(nodes):
    if len(nodes) < 2:
        print('Se requieren al menos 2 nodos')
        return
    clear()
    printNodes(nodes)
    try:
        node1=input('Introduzca ID del nodo que va a encriptar el mensaje: ')
        node2=input('Introduzca el ID del nodo que va a recibir el mensaje: ')
        filename=input('Introduzca el nombre del mensaje: ')
        message=input('Introduzca el mensaje a encriptar: ')
        nodes[node1].encryptMessage(nodes[node2],message,filename)
    except:
        print('Ocurrio una excepcion, revise sus entradas')

def decrypt(nodes):
    if len(os.listdir('Messages')) == 0:
        print('No hay mensajes que leer')
        return
    clear()
    printNodes(nodes)
    try:
        node=input('Introduzca la ID del nodo que leera el mensaje: ')
        clear()
        printFiles()
        file=input('Introduzca nombre del archivo para desencriptar: ')
        nodes[node].decryptMessage(file);
    except:
        print('Ocurrio una excepcion, revise sus entradas')

def main():
    nodes={}
    opc=0
    try:
        os.mkdir('Messages')
    except:
        shutil.rmtree('Messages')
        os.mkdir('Messages')
    while opc != 4:
        try:
            clear()
            print('0 - Añadir Nodo RSA')
            print('1- Eliminar Nodo RSA')
            print('2- Encriptar mensaje')
            print('3- Desencriptar mensaje')
            print('4- Terminar demonstracion')
            opc=int(input('Seleccione una opcion: '))
            if opc==0:
                addNode(nodes)
            elif opc==1:
                removeNode(nodes)
            elif opc==2:
                encrypt(nodes)
            elif opc==3:
                decrypt(nodes)
            elif opc==4:
                print('Terminando programa')
            else:
                print('Introduzca una opcion valida')
            input('Presione ENTER para continuar')
        except:
            print('Se genero una excepcion, intente de nuevo')

if __name__ == '__main__':
    main()
