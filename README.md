# RSA_MatematicasComputacionales

Codigo y Documentacion para la implementacion de RSA en python hecha para la clase de Matematicas computacionales

## primes.py

Este modulo se encarga de generar numeros primos

### Metodos

1. generate_prime_number
  - Genera un numero primo de n bits genrando candidatos y usando pruebas de primalidad para determinar el numero a regresar
  - Argumentos:
    - length: variable de tipo entero, longitud en bits del numero a generar, valor por defecto=128

2. generate_prime_candidate
  - Genera candidatos a numeros primos de n bits, el metodo usado es generar numeros de n bits aleatorios y luego igualar el bit mas significativo y el bit menos significativo a 1
  - Argumentos:
    - length: variable de tipo entero, longitud del numero a generar

3. is_prime
  - Realiza la prueba de primalidad de Fermat k veces al numero n, regresa True si es primo y False si no
  - Argumentos:
    - n:
      - Variable de tipo entero
      - Candidato a numero primo
    - k:
      - Variable de tipo entero
      - Numero de veces a realizar la prueba de primalidad de Fermat
      - Valor por defecto=128

## rsa.py

Este modulo contiene el codigo para los objetos de nodo RSA, asi como la interfaz maquina-hombre

### RSAClient

Clase de python para la creacion y manejo de nodos RSA

#### Variables

  1. keysize:
    - Variable protegida de tipo entero
    - Contiene el tamaño en bits de los numeros p y q usados para generar las llaves
  2. id:
    - Varible protegida de tipo string
    - Guarda el nombre del nodo
  3. keys:
    - Variable protegida de tipo lista
    - Guarda las llaves publica y privada

#### Metodos

1. init
  - Funcion de inicializacion de objeto de clase RSAClient
  - Argumentos:
    - id:
      - Variable de tipo string
      - Valor al que se inicializara la variable de clase id
    - keysize:
      - Variable de tipo entero
      - Tamaño de los primos p y q que se usaran para determinar las llaves
      - Valor por defecto=10
      - En una aplicacion real se utilizaria un tamaño de al menos 128 bits pero se selecciono un valor pequeño para velocidad de computo en la demonstracion

2. getID
  - Funcion que regresa la variable de clase id

3. decryptMessage
  - Funcion que desencripta e imprime un mensaje guardado en un archivo
  - Argumentos:
    - filename
      - Se buscara el mensaje en Messages/filename
      - Valor predeterminado=data.txt

4. encryptMessage
  - Funcion que encripta un mensaje y lo guarda en un archivo para su posterior lectura
  - Argumentos:
    - partner:
      - Variable de tipo RSAClient
      - Nodo al que se dirige el mensaje a encriptar
    - message:
      - Variable de tipo string
      - Mensaje a encriptar
    - filename:
      - Variable de tipo string
      - Direccion en Messages/filename donde se guardara el Mensaje

5. getPublicKey
  - Funcion que regresa la llave publica del nodo

6. findCoprime
  - Funcion que regresa un numero e coprimo a n
    - Argumentos:
      - n:
        - Variable de tipo entero
        - e sera un numero coprimo a n
      - size:
        - Variable de tipo entero
        - tamaño en bits del numero e a regresar
        - valor por defecto=16

7. modInverse
  - Funcion que regresa la inversa modular de a % b
    - Argumentos:
    - a:
      - Variable de tipo entero
      - Dividendo en la operacion a % b
    - b
      - Variable de tipo entero
      - Divisor en la operacion a % b

8. gcd
  - Funcion recursiva que regresa el maximo comun denominador de a y b, en caso de que b sea igual a cero se regresa a , de lo contrario se hace llamar gcd(b, a%b)
    - Argumentos:
      - a:
        - Variable de tipo entero
      - b:
        - Variable de tipo entero

9. createKeys
  - Funcion que crea y regresa las llaves publica y privada

### Interfaz maquina-hombre

#### Variables

1. nodes
  - Variable de tipo diccionario
  - La llave es de tipo string y corresponde al id de cada nodo creado
  - El valor sera un nodo de clase RSAClient
  - se incializa  vacio

#### Metodos

1. clear
  - Funcion que limpia la consola

2. printNodes
  - Funcion que imprime los valores de un diccionario
    - Argumentos:
      - nodes:
        - Variable de tipo diccionario
        - Se imprimen todos los valores existentes en el diccionario

3. printFiles
  - Funcion que imprime todos los archivos existentes en el directorio Messages/

4. addNode
  - Funcion que añade un par llave y valor a un diccionario
  - La funcion pide al usuario un valor de id para crear un nodo de clase RSAClient y añadirlo al diccionario
  - Argumentos:
    - nodes:
      - Variable de tipo diccionario
      - Diccionario donde se añadira el par llave y Valor

5. removeNode
  - Funcion que pide al usuario una id de nodo y remueve el valor correspondiente en un Diccionario
    - Argumentos:
      - nodes:
        - Variable de tipo diccionario
        - Diccionario de donde se removera un par llave y valor

6. encrypt
  - Funcion que le pide al usuario un id de origen, destino, filename, y mensaje
  - Utiliza los id para conseguir nodos de Clase RSAClient de un diccionario
  - Hace llamar origen.encryptMessage(destino,mensaje,filename)
  - En caso de que el usuario ingrese datos incorrectos se genera una excepcion
  - Argumentos:
    - nodes:
      - Variable de tipo diccionario
      - De aqui se extraen los nodos de origen y destino

7. decrypt
  - Funcion que le pide al usuario que ingrese un id de nodo, y filename
  - Extrae el valor correspondiente al id de nodo de un diccionario
  - Hace llamar nodo.decryptMessage(filename)
  - Argumentos:
    - nodes:
      - Variable de tipo diccionario
      - diccionario de donde se extrae el nodo correspondiente al id proporcionado

8. main
  - Funcion principal del programa
  - Se genera la variable nodes
  - Se crea el directorio Messages/ para guardar los mensajes
  - Se imprime el menu principal y se pide que el usuario seleccione alguna opcion
